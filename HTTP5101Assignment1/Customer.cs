﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTTP5101Assignment1
{
    public class Customer
    {
        //Private variables 
        private string customerEmail;
        private string customerPhone;
        private string customerFullName;

        //Empty Constructor 
        public Customer()
        {

        }

        //Property Accessors
        public string CustomerFullName
        {
            get
            {
                return customerFullName;
            }
            set
            {
                customerFullName = value;
            }
        }

        public string CustomerEmail
        {
            get
            {
                return customerEmail;
            }
            set
            {
                customerEmail = value;
            }
        }

        public string CustomerPhone
        {
            get
            {
                return customerPhone;
            }
            set
            {
                customerPhone = value;
            }
        }

    }

}