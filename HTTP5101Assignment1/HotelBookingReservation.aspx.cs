﻿/*
Jing Cheng
N01289618
Assignment 1b

New files:
Customer.cs
Room.cs
Booking.cs

Requirements Checklist
- Public class with a constructor
    - Booking.cs LINE 16
    - Hotel.cs LINE 27

- A private class with a property accessor
    - Customer.cs

- A field of data type integer
    - guestsPerRoom (HotelBookingReservation.aspx.cs LINE 53)

- A field of data type string
    - numberOfRooms (HotelBookingReservation.aspx.cs LINE 50)
    - Customer (HotelBookingReservation.aspx.cs LINE 67-76)

- A field of data type list
    - optionalAmenities (HotelBookingReservation.aspx.cs LINE 83-100)

- One of *DateTime*
    - checkInDate and checkOutDate (HotelBookingReservation.aspx.cs LINE 65-70)

- Encapsulation 
    - Booking.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace HTTP5101Assignment1
{
    public partial class HotelBookingReservation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //Function fires on press of submit button
        protected void RoomBooking(object sender, EventArgs e)
        {
            //Set up data types for Hotel
            List<String> amenities = new List<String>
            {

            };

            //Pull hotelRating from form
            //int hotelRating = int.Parse(hotelStarQuality.SelectedValue);

            //Pull numberOfRooms from form
            string numberOfRooms = rooms.SelectedItem.Value;

            //Pull guestsPerRoom from form
            int guestsPerRoom = int.Parse(numberOfPeople.SelectedItem.Value);

            //DateTime Parse referenced from
            //https://www.dotnetperls.com/datetime-parse
            DateTime checkInDate;
            checkInDate = DateTime.Parse(CheckInDate.Text).Date;

            DateTime checkOutDate;
            checkOutDate = DateTime.Parse(CheckOutDate.Text).Date;

            //Instantiate Room
            Room roomBooking = new Room(amenities, /*hotelRating,*/ numberOfRooms, guestsPerRoom, checkInDate, checkOutDate);

            //Set up data types for customer
            string customerName = customerFullName.Text.ToString();
            string customerElectronicMail = customerEmail.Text.ToString();
            string customerPhoneNumber = customerPhone.Text.ToString();

            //Instantiate Customer 
            Customer newCustomer = new Customer();
            newCustomer.CustomerFullName = customerName;
            newCustomer.CustomerEmail = customerElectronicMail;
            newCustomer.CustomerPhone = customerPhoneNumber;

            //Add extra amenities to room, first create list
            List<string> optionalAmenities = new List<string>();

            //Code to fetch data from checkbox and add it to a list
            //Referenced from Christine Bittle
            //@bitbucket.org/salamanderburger/http5101-server-control-intro.git
            foreach (Control control in amenitiesContainer.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox amenity = (CheckBox)control;
                    if (amenity.Checked)
                    {
                        optionalAmenities.Add(amenity.Text);
                    }

                }
            }

            //Concat our amenities from checked checkboxes to our amenities list
            roomBooking.amenities = roomBooking.amenities.Concat(optionalAmenities).ToList();

            //Instantiate new instance of Booking class with arguements being instantiated hotel and customer class
            Booking newBooking = new Booking(roomBooking, newCustomer);

            //Change div HTML to output our receipt
            BookingReceiptOutput.InnerHtml = newBooking.BookingReceipt();

        }
    }
}