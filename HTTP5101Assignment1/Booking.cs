﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTTP5101Assignment1
{
    public class Booking
    {
        //Booking class uses both the hotel and customer classes
        //Instantiate classes
        public Room room;
        public Customer customer;
        
        //Constructor
        public Booking(Room h, Customer c)
        {
            room = h;
            customer = c;
        }

        //Output our receipt
        public string BookingReceipt()
        {
            string bookingReceipt = "<br/" + "Booking Receipt: </br>";
            bookingReceipt += "Total: " + CalculateBooking().ToString() + "<br/>";
            bookingReceipt += "Name: " + customer.CustomerFullName + "<br/>";
            bookingReceipt += "Phone Number: " + customer.CustomerPhone + "<br/>";
            bookingReceipt += "Email: " + customer.CustomerEmail + "<br/>";

            //bookingReceipt += "Hotel Star Rating: " + hotel.stars + "<br/>";
            bookingReceipt += "Number of rooms: " + room.rooms + "<br/>";
            bookingReceipt += "People per room: " + room.guests + "<br/>";

            bookingReceipt += "Additional Amenities: " + String.Join(", ", room.amenities.ToArray()) + "<br/>";

            bookingReceipt += "Check in date: " + room.checkInDate + "<br/>";
            bookingReceipt += "Check out date: " + room.checkOutDate + "<br/>";

            return bookingReceipt;
        }

        //Calculate Price based on hotel rating, number of rooms, number of guests and total days 
        public double CalculateBooking()
        {
            double totalDays;
            double total = 100;
            double tax = 1.13;
            //if(hotel.stars == 5)
            //{
            //    total += 200;
            //}
            //else if(hotel.stars == 4)
            //{
            //    total += 150;
            //}
            //else if(hotel.stars == 3)
            //{
            //    total += 100;
            //}
            //else if(hotel.stars == 2)
            //{
            //    total += 80;
            //}
            //else if(hotel.stars == 1)
            //{
            //    total += 50;
            //}

            if(room.rooms == "2")
            {
                total *= 2;
            }
            else if(room.rooms == "3")
            {
                total *= 3;
            }
            else if(room.rooms == "4")
            {
                total *= 4;
            }

            if(room.guests == 2)
            {
                total *= 1.1;
            }
            else if(room.guests == 3)
            {
                total *= 1.2;
            }
            else if(room.guests == 4)
            {
                total *= 1.3;
            }
        
        //Realized that total was not factoring in dates and wanted to figure out how to do so
        //.TotalDays referenced from 
        //https://www.c-sharpcorner.com/article/calculating-date-difference-in-C-Sharp/
            totalDays = (room.checkOutDate - room.checkInDate).TotalDays;
            total *= totalDays;
            total *= tax;
            return total;
        }


    }
}