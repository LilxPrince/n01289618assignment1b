﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTTP5101Assignment1
{
    public class Room
    {
        //DateTime for check in and check out dates
        public DateTime checkInDate;
        public DateTime checkOutDate;

        //List for amenities
        public List<string> amenities;

        //Hotel rating star
        //public int stars;

        //Number of rooms 
        public string rooms;

        //Number of people per room
        public int guests;

        //Constructor
        public Room(List<string>a, /*int s, */ string r, int g, DateTime ci, DateTime co)
        {
            //Set our arguements and assign them to the class's variables
            amenities = a;
            //stars = s;
            rooms = r;
            guests = g;
            checkInDate = ci;
            checkOutDate = co;
        }

        


    }
}