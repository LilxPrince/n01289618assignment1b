﻿<%--
Jing Cheng
Student#: n01289618
Additional Notes:
    - Assignment includes:
        - 5 Input fields
        - 6 Checkbox fields
        - 4 Radio button fields
        - 1 Drop down list
        - 1 Submit button

        - 5 Required field validators (asp:TextBoxs)
        - 2 Regular expression validators (E-mail and Phone Number)
        - 2 Range validators (Check-in and Check-out dates)
        - 1 Validation summary
--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBookingReservation.aspx.cs" Inherits="HTTP5101Assignment1.HotelBookingReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Booking Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>The Jingaling Hotel Booking Form</h1>
            <%--Check in date Textbox--%>
            <br />
            <label for="Check in date">Check In Date: </label>
            <asp:TextBox runat="server" ID="CheckInDate" placeholder="YYYY/MM/DD"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="checkindaterequired" ControlToValidate="CheckInDate" ForeColor="Red" ErrorMessage="You must enter a valid check-in date"></asp:RequiredFieldValidator>
            <%--Unfortunate that by default date format for range validator is set to yyyy/mm/dd--%>
            <asp:RangeValidator runat="server" ID="checkinvalidator" ControlToValidate="CheckInDate" Type="Date" MinimumValue="2018/01/01" MaximumValue="2018/12/31" ForeColor="Red" ErrorMessage="You must enter a valid date within 2018 (example format: 2018/01/01)"></asp:RangeValidator>

            <%--Check out date Textbox--%>
            <br />
            <label for="Check out date">Check Out Date: </label>
            <asp:TextBox runat="server" ID="CheckOutDate" placeholder="YYYY/MM/DD"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="checkoutdatevalidator" ControlToValidate="CheckOutDate" ForeColor="Red" ErrorMessage="You must enter a valid check-out date"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="checkoutvalidator" ControlToValidate="CheckOutDate" Type="Date" MinimumValue="2018/01/01" MaximumValue="2018/12/31" ForeColor="Red" ErrorMessage="You must enter a valid date within 2018 (example format: 2018/01/01"></asp:RangeValidator>
        

            <%--Unspecified Dates--%>
            <br />
            <asp:CheckBox runat="server" ID="unspecified" Text="I don't have specific dates yet"/>

            <%--Star Rating--%>
<%--            <br />
            <br />
            <label for="Hotel Star Quality">Hotel Star Quality: </label>
            <asp:RadioButtonList runat ="server" ID ="hotelStarQuality">
                <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                <asp:ListItem Text="4" Value="4">4</asp:ListItem>
                <asp:ListItem Text="5" Value="5">5</asp:ListItem>
            </asp:RadioButtonList>--%>

            <%--Room DDL--%>
            <br />
            <br />
            <label for="Rooms">Rooms: </label>
            <asp:DropdownList ID="rooms" runat="server">
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
            </asp:DropdownList>

            <%--People--%> 
            <br />
            <br />
            <label for="People per room">People per room:</label>
            <asp:RadioButtonList runat ="server" ID ="numberOfPeople">
                <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                <asp:ListItem Text="4" Value="4">4</asp:ListItem>
            </asp:RadioButtonList>

            <%--Hotel amenities--%>
            <br />
            <label for="Hotel amenities">Optional amenities:</label>
            <div id="amenitiesContainer" runat="server">
                <asp:CheckBox runat="server" ID="amenity1" Text="Extra Towels"/>
                <asp:CheckBox runat="server" ID="amenity2" Text="De-Humidifier"/>
                <asp:CheckBox runat="server" ID="amenity3" Text="Extra Lamp"/>
                <asp:CheckBox runat="server" ID="amenity4" Text="Extra chairs" />
            </div>

            <%--Personal Information--%>
            <br />
            <br />
            <label for="Customers Full Name">Full Name:</label>
            <asp:TextBox runat="server" ID="customerFullName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="namerequired" ControlToValidate="customerFullName" ErrorMessage="You must enter a name" ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            <label for="Customers Email">Email: </label>
            <asp:TextBox runat="server" ID="customerEmail" placeholder="E-mail"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="emailrequired" ControlToValidate="customerEmail" ErrorMessage="You must enter a email" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="emailvalidator" ControlToValidate="customerEmail" ErrorMessage="You must enter a valid e-mail address" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <label for="Customers Phone Number">Phone Number: </label>
            <asp:TextBox runat="server" ID="customerPhone" placeholder="Phone Number"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="phonerequired" ControlToValidate="customerPhone" ErrorMessage="You must enter a phone number" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="phonevalidation" ControlToValidate="customerPhone" ErrorMessage="You must enter a valid phone number (example format: 123-123-1234)" ForeColor="Red" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>

            <%--Submit--%>
            <br />
            <br />
            <asp:Button runat="server" ID="button" OnClick="RoomBooking" Text="Submit" />

            <%--Validation Summary--%>
            <asp:ValidationSummary ID="validationsummary" ForeColor="Red" runat="server" />

            <div runat = "server" id ="BookingReceiptOutput">


            </div>

        </div>
    </form>
</body>
</html>
